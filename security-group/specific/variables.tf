variable "cidr_blocks" {
    description = "A list of cidr_blocks granting access to AWS resouces"
    type        = list(string)
}

variable "ports" {
    description = "A list of ports to open access to AWS resouces"
    type        = list(string)
}

variable "vpc_id" {
    description = "A VPC ID to attach the security group to"
    type        = string
}

variable "name_prefix" {
    description = "A name prefix for the security group"
    type        = string
}

variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}