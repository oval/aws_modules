resource "aws_security_group" "sg" {
  name_prefix = var.name_prefix
  vpc_id      = var.vpc_id
  tags        = {
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}

resource "aws_security_group_rule" "ingress_ext" {
  count             = length(var.ports)
  type              = "ingress"
  from_port         = var.ports[count.index]
  to_port           = var.ports[count.index]
  protocol          = "tcp"
  cidr_blocks       = var.cidr_blocks
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "ingress_self" {
  count             = length(var.ports)
  type              = "ingress"
  from_port         = var.ports[count.index]
  to_port           = var.ports[count.index]
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.sg.id
}

resource "aws_security_group_rule" "egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.sg.id
}