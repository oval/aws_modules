Param(
    # The commit message to use
    [Parameter(Mandatory=$True)]
    [String]
    $Msg
)

$current = git tag --list | Foreach-Object {
    $lastDot = $_.LastIndexOf(".");
    if( $lastDot -ge 1 ) {
      $_.SubString($lastDot+1)
    }
  } | Foreach-Object {
    if( ($_ -as [int]) -ne $null ) {
      [int]$_
    }
  } | Sort-Object | Select-Object -Last 1
  
  $next = $current + 1
  
  git commit -m $Msg
  if($?) {
    git tag -a "0.$next" -m $Msg
    git push origin master --follow-tags
  }