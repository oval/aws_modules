resource "aws_kms_key" "default" {
  description = "KMS create for encrypting ${var.name}"
  policy      = var.key_policy
  
  enable_key_rotation = true
}