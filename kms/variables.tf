variable "name" {
  description = "Name of the resource the key will be used for"
  type        = string
}

variable "key_policy" {
  description = "A json key policy to attach to the key"
  type        = string
}