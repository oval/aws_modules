variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "name_prefix" {
  description = "A prefix to add to the load balancer name"
  type        = string
}

variable "security_groups" {
  description = "A list of sercurity groups to provide load balancer resource access"
  type        = list(string)
}

variable "subnets" {
  description = "A list of subnets the load balancer can reside in"
  type        = list(string)
}

variable "deletion_protection" {
  default     = "false"
  description = "Do you want to enable deletion protection for the load balancer"
  type        = string

  validation {
    condition     = can(regex("(true|false)", var.deletion_protection))
    error_message = "The deletion_protection value must be true or false."
  }
}

variable "access_logs_bucket" {
  description = "the bucket where access logs need to be stored"
  type        = string
}

variable "access_logs_prefix" {
  description = "The key prefix for the access logs"
  type        = string
}