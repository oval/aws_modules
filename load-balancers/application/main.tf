resource "aws_lb" "main" {
  name_prefix                = var.name_prefix
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = var.security_groups
  subnets                    = var.subnets
  enable_deletion_protection = var.deletion_protection
  
  access_logs {
    bucket  = var.access_logs_bucket
    prefix  = var.access_logs_prefix
    enabled = true
  }

  tags = {
    Project     = var.project_tag
    Environment = var.environment_tag
  }
}

module "http_redirect" {
  source = "../listeners/redirect"

  alb_arn     = aws_lb.main.arn
  port        = 443
  protocol    = "HTTPS"
  status_code = "HTTP_301"
}