variable "alb_arn" {
  description = "The Load balancer arn to attach the listener to"
  type        = string
}

variable "port" {
  description = "The port number to redirect requests to"
  type        = string
}

variable "protocol" {
  description = "The port protocol for redirect requests, must "
  type        = string

  validation {
    # regex(...) fails if it cannot find a match
    condition     = can(regex("(HTTP|HTTPS)", var.protocol))
    error_message = "The protocol value must be HTTP or HTTPS."
  }
}

variable "status_code" {
  default     = "HTTP_301"
  description = "The status code to send when the request is redirected. Default = HTTP_301"
  type        = string

  validation {
    # regex(...) fails if it cannot find a match
    condition     = can(regex("(HTTP_301|HTTP_302)", var.status_code))
    error_message = "The status_code value must be permanent (HTTP_301) or temporary (HTTP_302)."
  }
}