resource "aws_lb_listener" "https" {
  load_balancer_arn = var.alb_arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = var.port
      protocol    = var.protocol
      status_code = var.status_code
    }
  }
}