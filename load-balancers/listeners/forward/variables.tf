variable "alb_arn" {
  description = "The Load balancer arn to attach the listener to"
  type        = string
}

variable "ssl_policy" {
  default     = "ELBSecurityPolicy-TLS-1-2-2017-01"
  description = "The SSL policy to apply to the listener. Default = ELBSecurityPolicy-TLS-1-2-2017-01"
  type        = string
}

variable "certificate_arn" {
  description = "The SSL certificate arn to attach to the listener"
  type        = string
}

variable "target_group_arn" {
  description = "The target group arn to forward requests to"
  type        = string
}