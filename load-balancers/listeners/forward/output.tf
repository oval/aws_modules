output "id" {
    value = aws_lb_listener.https.id
}

output "arn" {
    value = aws_lb_listener.https.arn
}