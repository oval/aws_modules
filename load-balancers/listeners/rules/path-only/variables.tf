variable "listener_arn" {
    description = "The arn of the listener to add the rule to"
    type        = string
}

variable "priority" {
    description = "The prioprity level of the listener rule"
    type        = number
}

variable "target_group_arn" {
    description = "The arn of the target group that the rule forwards to"
    type        = string
}

variable "path_values" {
    description = "A list of path values for the rule to listen to"
    type        = set(string)
}