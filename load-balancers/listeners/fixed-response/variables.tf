variable "alb_arn" {
  description = "A Project tag"
  type        = string
}

variable "ssl_policy" {
  default     = "ELBSecurityPolicy-TLS-1-2-2017-01"
  description = "The SSL policy to apply to the listener. Default = ELBSecurityPolicy-TLS-1-2-2017-01"
  type        = string
}

variable "certificate_arn" {
  description = "The SSL certificate arn to attach to the listener"
  type        = string
}

variable "message" {
  description = "A message to return in the response object"
  type        = string
}

variable "status_code" {
  description = "A status code to return with the response. Must be a 2XX, 4XX or 5XX code"
  type        = number
}