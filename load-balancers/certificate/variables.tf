variable "load_balancer_arn" {
  description = "The load balancer arn"
  type        = string
}

variable "acm_certificate_arn" {
  description = "The regional certificate to attach"
  type        = string
}