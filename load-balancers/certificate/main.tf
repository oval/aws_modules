resource "aws_lb_listener_certificate" "cert" {
  listener_arn    = var.load_balancer_arn
  certificate_arn = var.acm_certificate_arn
}