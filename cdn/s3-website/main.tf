resource "aws_cloudfront_distribution" "website" {
  origin {
    domain_name = var.bucket_domain_name
    origin_id   = var.s3_origin_id
  }
  aliases             = var.aliases
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = var.index_document
  price_class         = "PriceClass_All"

  logging_config {
    include_cookies = false
    bucket          = var.log_storage_domain
    prefix          = "cdn_logs"
  }
  
  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = var.s3_origin_id
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    
    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  ordered_cache_behavior {
    path_pattern     = "index.html"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = var.s3_origin_id

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }

  viewer_certificate {
    acm_certificate_arn      = var.certificate_arn
    minimum_protocol_version = var.ssl_policy
    ssl_support_method       = "sni-only"
  }

  custom_error_response {
    error_caching_min_ttl = 10
    error_code            = 404
    response_page_path    = "/${var.index_document}"
    response_code         = 200
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}