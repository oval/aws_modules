variable "bucket_domain_name" {
  description = "The domain name of the bucket that the CDN acts on"
  type        = string
}

variable "s3_origin_id" {
  description = "A unique name for the origin id of the CDN"
  type        = string
}

variable "index_document" {
  default     = "index.html"
  description = "The website index document. Default = 'index.html'"
  type        = string
}

variable "ssl_policy" {
  default     = "TLSv1.2_2021"
  description = "The minimum ssl policy for the CDN. Default = 'TLSv1.2_2021'"
  type        = string
}

variable "log_storage_domain" {
  description = "The domain name of the bucket for access logs"
  type        = string
}

variable "project_tag" {
  description = "A project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "aliases" {
  description = "A list if alternate CNAMEs"
  type        = list(string)
}

variable "certificate_arn" {
  description = "The certificate arn for a US-EAST-1 SSL certificate"
  type        = string
}