resource "aws_cloudfront_distribution" "website" {
  origin {
    domain_name = var.bucket_domain_name
    origin_id   = var.s3_origin_id
  }
  aliases             = var.aliases
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = var.index_document
  price_class         = "PriceClass_All"

  logging_config {
    include_cookies = false
    bucket          = var.log_storage_domain
    prefix          = "cdn_logs"
  }
  
  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = var.s3_origin_id
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    
    forwarded_values {
      query_string = false
      headers      = ["origin","access-control-request-headers","access-control-request-method"]

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = var.lambda_edge_arn
      include_body = false
    }
  }

  ordered_cache_behavior {
    path_pattern     = "index.html"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD", "OPTIONS"]
    target_origin_id = var.s3_origin_id

    forwarded_values {
      query_string = false
      headers      = ["origin","access-control-request-headers","access-control-request-method"]

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = var.lambda_edge_arn
      include_body = false
    }

    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }

  viewer_certificate {
    acm_certificate_arn      = var.certificate_arn
    minimum_protocol_version = var.ssl_policy
    ssl_support_method       = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}