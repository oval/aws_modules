resource "aws_cloudfront_origin_access_control" "default" {
  name             = "${substr(var.bucket_domain_name, 0, 32)}-OAC"
  description      = "OAC for ${var.bucket_domain_name}"
  signing_behavior = "always"
  signing_protocol = "sigv4"

  origin_access_control_origin_type = "s3"
}

resource "aws_cloudfront_distribution" "website" {
  origin {
    domain_name = var.bucket_domain_name
    origin_id   = var.s3_origin_id

    origin_access_control_id = aws_cloudfront_origin_access_control.default.id
  }
  aliases             = var.aliases
  enabled             = true
  is_ipv6_enabled     = true
  price_class         = "PriceClass_All"

  logging_config {
    include_cookies = false
    bucket          = var.log_storage_domain
    prefix          = "cdn_logs"
  }
  
  default_cache_behavior {
    allowed_methods        = ["GET", "HEAD", "OPTIONS"]
    cached_methods         = ["GET", "HEAD"]
    target_origin_id       = var.s3_origin_id
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    
    forwarded_values {
      query_string = false
      headers      = ["origin","access-control-request-headers","access-control-request-method"]

      cookies {
        forward = "none"
      }
    }

    lambda_function_association {
      event_type = "origin-response"
      lambda_arn = var.lambda_edge_origin_response_arn
      include_body = false
    }

    lambda_function_association {
      event_type = "origin-request"
      lambda_arn = var.lambda_edge_origin_request_arn
      include_body = false
    }
  }

  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }

  viewer_certificate {
    acm_certificate_arn      = var.certificate_arn
    minimum_protocol_version = var.ssl_policy
    ssl_support_method       = "sni-only"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
}