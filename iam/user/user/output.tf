output "id" {
  value = aws_iam_user.user.unique_id
}

output "name" {
  value = aws_iam_user.user.name
}

output "arn" {
  value = aws_iam_user.user.arn
}