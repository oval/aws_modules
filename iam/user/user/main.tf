resource "aws_iam_user" "user" {
  path = "/"
  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }
  name = var.name

}

resource "aws_iam_access_key" "user_access_key" {
  user = aws_iam_user.user.name
}

resource "aws_iam_user_policy_attachment" "attachments" {
  count      = length(var.policy_arns)
  user       = aws_iam_user.user.name
  policy_arn = var.policy_arns[count.index]
}