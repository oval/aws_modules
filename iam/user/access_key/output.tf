output "aws_iam_access_key" {
  value = aws_iam_access_key.user_access_key.id
}

output "aws_iam_access_key_secret" {
  value = aws_iam_access_key.user_access_key.secret
}