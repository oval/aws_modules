variable "name_prefix" {
  description = "A name prefixx to use with the role"
  type        = string
}

variable "role_name" {
  description = "A role name to assign to the instance profile"
  type        = string
}
