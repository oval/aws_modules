output "id" {
  value = aws_iam_instance_profile.main.id
}

output "arn" {
  value = aws_iam_instance_profile.main.arn
}