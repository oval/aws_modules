resource "aws_iam_instance_profile" "main" {
  name_prefix = var.name_prefix
  path        = "/"
  role        = var.role_name
}