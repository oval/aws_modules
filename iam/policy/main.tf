resource "aws_iam_policy" "main" {
  name_prefix = var.name_prefix
  description = var.description
  path        = "/"
  policy      = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = var.actions
        Effect = "Allow"
        Resource = var.resources
      }
    ]
  })
}