variable "name_prefix" {
  description = "A name prefix to use on the policy"
  type        = string
}

variable "description" {
  default     = ""
  description = "A description to use with the poilcy"
  type        = string
}

variable "actions" {
  description = "A list of actions for the policy to allow"
  type        = list(string)
}

variable "resources" {
  description = "A list of resources for the actions to have an affect on"
  type        = list(string)
}