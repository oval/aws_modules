resource "aws_iam_role" "main" {
  name_prefix         = var.name_prefix
  assume_role_policy  = var.assume_role_policy_document
  managed_policy_arns = var.policy_arns

  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }
}