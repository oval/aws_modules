variable "bucket_name" {
  description = "The bucket name for the logs"
  type        = string
}

variable "logs_prefix" {
  description = "The alb logs prefix"
  type        = string
}

variable "region" {
  description = "The region of the load balancer"
  type        = string
}

variable "account_id" {
  description = "The account id where the alb resides"
  type        = string
}