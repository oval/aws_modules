locals {
  alb_id_map = {
    "us-east-1": "127311923021",
    "us-east-2": "033677994240",
    "us-west-1": "027434742980",
    "us-west-2": "797873946194",
    "af-south-1": "098369216593",
    "ca-central-1": "985666609251",
    "eu-central-1": "054676820928",
    "eu-west-1": "156460612806",
    "eu-west-2": "652711504416",
    "eu-south-1": "635631232127",
    "eu-west-3": "009996457667",
    "eu-north-1": "897822967062",
    "ap-east-1": "754344448648",
    "ap-northeast-1": "582318560864",
    "ap-northeast-2": "600734575887",
    "ap-northeast-3": "383597477331",
    "ap-southeast-1": "114774131450",
    "ap-southeast-2": "783225319266",
    "ap-south-1": "718504428378",
    "me-south-1": "076674570225",
    "sa-east-1": "507241528517",
  }
}

resource "aws_s3_bucket_policy" "p" {
  bucket = var.bucket_name
  policy = jsonencode({
      "Version": "2008-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Principal": {
                  "AWS": "arn:aws:iam::${lookup(local.alb_id_map, var.region, "0")}:root"
              },
              "Action": "s3:PutObject",
              "Resource": "arn:aws:s3:::${var.bucket_name}/${var.logs_prefix}/AWSLogs/${var.account_id}/*"
          },
          {
              "Effect": "Allow",
              "Principal": {
                  "Service": "delivery.logs.amazonaws.com"
              },
              "Action": "s3:PutObject",
              "Resource": "arn:aws:s3:::${var.bucket_name}/${var.logs_prefix}/AWSLogs/${var.account_id}/*",
              "Condition": {
                  "StringEquals": {
                      "s3:x-amz-acl": "bucket-owner-full-control"
                  }
              }
          },
          {
              "Effect": "Allow",
              "Principal": {
                  "Service": "delivery.logs.amazonaws.com"
              },
              "Action": "s3:GetBucketAcl",
              "Resource": "arn:aws:s3:::${var.bucket_name}"
          }
      ]
  })
}