variable "project_tag" {
  description = "A project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "image_uri" {
  description = "The ecr image reporitory name with tag"
  type        = string
}

variable "function_name" {
  description = "A unique name for the function"
  type        = string
}

variable "lambda_role_arn" {
  description = "A role arn for the function to use"
  type        = string
}

variable "timeout" {
  description = "The max timeout for the lambda function"
  type        = number
}

variable "env_vars" {
  description = "A map of environment variables for the lambda function to access"
  type        = map(string)
}

variable "memory_size" {
  description = "The max memory allocation for the function"
  type        = number
}