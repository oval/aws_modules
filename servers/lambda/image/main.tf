resource "aws_lambda_function" "index" {
  image_uri     = var.image_uri
  function_name = var.function_name
  role          = var.lambda_role_arn
  timeout       = var.timeout
  package_type  = "Image"
  memory_size   = var.memory_size

  environment {
    variables = var.env_vars
  }

  tags = {
    Project     = var.project_tag
    Environment = var.environment_tag
  }
}