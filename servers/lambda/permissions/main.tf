resource "aws_lambda_permission" "permission" {
  action        = "lambda:InvokeFunction"
  principal     = var.principal
  function_name = var.function_arn
  source_arn    = var.source_arn
}