variable "principal" {
  description = "The prinicple resource for the permissions"
  type        = string
}

variable "function_arn" {
  description = "A function arn for the permissions"
  type        = string
}

variable "source_arn" {
  description = "A source arn for the function to provide permission"
  type        = string
}
