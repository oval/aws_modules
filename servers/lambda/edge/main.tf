terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


resource "aws_lambda_function" "edge" {
  function_name = "${var.function_prefix}-edgefn"
  description   = "Lambda@Edge function for the ${var.function_prefix} CDN"

  # Find the file from S3
  s3_bucket         = var.code_bucket
  s3_key            = var.lambda_code_key

  publish = true
  handler = var.lambda_handler
  runtime = var.lambda_runtime
  role    = aws_iam_role.lambda_at_edge.arn
  tags    = {
    Project = var.project_tag
    Environment = var.environment_tag
  }

  lifecycle {
    ignore_changes = [
      last_modified,
    ]
  }
}

data "aws_iam_policy_document" "assume_role_policy_doc" {
  statement {
    sid    = "AllowAwsToAssumeRole"
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type = "Service"

      identifiers = [
        "edgelambda.amazonaws.com",
        "lambda.amazonaws.com",
      ]
    }
  }
}

/**
 * Make a role that AWS services can assume that gives them access to invoke our function.
 * This policy also has permissions to write logs to CloudWatch.
 */
resource "aws_iam_role" "lambda_at_edge" {
  name               = "${var.function_prefix}-edgefn-role"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy_doc.json
  tags               = {
    Project = var.project_tag
    Environment = var.environment_tag
  }
}

/**
 * Allow lambda to write logs.
 */
data "aws_iam_policy_document" "lambda_logs_policy_doc" {
  statement {
    effect    = "Allow"
    resources = ["*"]
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",

      # Lambda@Edge logs are logged into Log Groups in the region of the edge location
      # that executes the code. Because of this, we need to allow the lambda role to create
      # Log Groups in other regions
      "logs:CreateLogGroup",
    ]
  }
}

/**
 * Attach the policy giving log write access to the IAM Role
 */
resource "aws_iam_role_policy" "logs_role_policy" {
  name   = "${var.function_prefix}-edgefn-policy"
  role   = aws_iam_role.lambda_at_edge.id
  policy = data.aws_iam_policy_document.lambda_logs_policy_doc.json
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "/aws/lambda/${var.function_prefix}-edgefn"

  retention_in_days = 90
}