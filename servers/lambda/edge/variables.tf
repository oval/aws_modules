variable "function_prefix" {
  description = "The a function name prefix, ideally the bucket name the CDN function will serve"
  type        = string
}

variable "code_bucket" {
  description = "The code bucket name where the edge code lives, this needs to be in the us-east-1 region"
  type        = string
}

variable "lambda_code_key" {
  description = "The S3 object key for the lambda code"
  type        = string
}

variable "lambda_handler" {
  description = "The lambda function handler"
  type        = string
}

variable "lambda_runtime" {
  description = "The lambda function runtime"
  type        = string
}

variable "project_tag" {
  description = "Project tag"
  type        = string
}

variable "environment_tag" {
  description = "Environment tag"
  type        = string
}