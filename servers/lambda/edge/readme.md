# Lambda@Edge function

Creates a Lambda@Edge function with a suitable role and policies. This function must be generated in the us-east-1 region.
