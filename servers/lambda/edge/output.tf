output "edge_function_arn" {
  value = aws_lambda_function.edge.arn
}

output "edge_function_version" {
  value = aws_lambda_function.edge.version
}