variable "sqs_queue_arn" {
  description = "The arn for the sqs queue"
  type        = string
}

variable "lambda_arn" {
  description = "The arn of the lambda function"
  type        = string
}