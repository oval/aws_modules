resource "aws_elastic_beanstalk_environment" "main" {
  name          = var.name
  application   = var.application_name
  description   = var.description
  tier          = var.tier
  template_name = var.configuration_template
  version_label = var.version_label

  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }

  lifecycle {
    ignore_changes = [
      version_label
    ]
  }
}