variable "name" {
  description = "A name for the environment"
  type        = string
}

variable "application_name" {
  description = "The application that environment will belong to"
  type        = string
}

variable "description" {
  default     = ""
  description = "A description for the environment"
  type        = string
}

variable "tier" {
  default     = "WebServer"
  description = "The Type of environment to build, must be either a 'WebServer' or 'Worker'. Default = 'WebServer'"
  type        = string

  validation {
    condition     = can(regex("(WebServer|Worker)", var.tier))
    error_message = "The tier must be either 'WebServer' or 'Worker'."

  }
}

variable "configuration_template" {
  description = "The configuration template to use to build the environment"
  type        = string
}

variable "project_tag" {
  description = "A project tag for the environment"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag for the environment"
  type        = string
}

variable "version_label" {
  description = "The application version to apply"
  type        = string
}