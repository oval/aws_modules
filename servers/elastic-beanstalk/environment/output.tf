output "id" {
    value = aws_elastic_beanstalk_environment.main.id
}

output "name" {
    value = aws_elastic_beanstalk_environment.main.name
}

output "cname" {
    value = aws_elastic_beanstalk_environment.main.cname
}

output "endpoint_url" {
    value = aws_elastic_beanstalk_environment.main.endpoint_url
}