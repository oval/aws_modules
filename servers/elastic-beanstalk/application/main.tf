resource "aws_elastic_beanstalk_application" "app" {
    name        = var.name
    description = var.description
}