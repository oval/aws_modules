variable "name" {
    description = "The name of the elastic beanstalk application"
    type        = string
}

variable "description" {
    default     = ""
    description = "A desription for the application"
    type        = string
}