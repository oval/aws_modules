variable "name_prefix" {
  description = "A name prefixx to use with the role"
  type        = string
}

variable "project_tag" {
  description = "A project tag to attach to the resource"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag to attach to the resource"
  type        = string
}

variable "policy_arns" {
  default     = []
  description = "A list of additional policy arns to add to the role. Default = []"
  type        = list(string)
}