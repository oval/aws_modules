module "role" {
  source = "../../../../iam/role"

  name_prefix     = var.name_prefix
  project_tag     = var.project_tag
  environment_tag = var.environment_tag

  assume_role_policy_document = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = "EC2AssumeRole"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  policy_arns = concat([
    "arn:aws:iam::aws:policy/AWSLambda_FullAccess",
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier",
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkMulticontainerDocker",
    "arn:aws:iam::aws:policy/AWSElasticBeanstalkWorkerTier"
  ], var.policy_arns)
}

module "instance_profile" {
  source = "../../../../iam/instance-profile"

  name_prefix = var.name_prefix
  role_name   = module.role.name
}