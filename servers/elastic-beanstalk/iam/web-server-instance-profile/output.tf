output "role_arn" {
  value = module.role.arn
}

output "role_id" {
  value = module.role.id
}

output "role_name" {
  value = module.role.name
}

output "instance_profile_arn" {
  value = module.instance_profile.arn
}

output "instance_profile_id" {
  value = module.instance_profile.id
}