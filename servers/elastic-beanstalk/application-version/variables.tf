variable "name" {
    description = "A name for the application version"
    type        = string
}

variable "application_name" {
    description = "The elastic beanstalk name for application version"
    type        = string
}

variable "description" {
    description = "A description for the apllication version"
    type        = string
}

variable "bucket" {
    description = "The S3 bucket where the code is stored"
    type        = string
}

variable "code_object_key" {
    description = "The S3 object key for the application code"
    type        = string
}