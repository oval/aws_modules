resource "aws_elastic_beanstalk_application_version" "main" {
  name        = var.name
  application = var.application_name
  description = var.description
  bucket      = var.bucket
  key         = var.code_object_key
}