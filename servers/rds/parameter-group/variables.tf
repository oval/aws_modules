variable "name_prefix" {
  description = "A name prefix for the parameter group"
  type        = string
}