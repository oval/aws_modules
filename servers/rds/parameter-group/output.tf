output "arn" {
  value = aws_rds_cluster_parameter_group.default.arn
}

output "id" {
  value = aws_rds_cluster_parameter_group.default.id
}