resource "aws_rds_cluster_parameter_group" "default" {
  name_prefix = var.name_prefix
  family      = "aurora-mysql5.7"
  description = "RDS Aurora MySQL cluster parameter group"
}