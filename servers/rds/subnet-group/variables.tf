variable "name_prefix" {
  description = "A name prefix for the subnet group"
  type        = string
}

variable "subnet_ids" {
  description = "A list of vpc subnet ids"
  type        = list(string)
}

variable "project_tag" {
  description = "A project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}