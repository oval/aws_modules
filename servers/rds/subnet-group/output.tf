output "arn" {
  value = aws_db_subnet_group.default.arn
}

output "id" {
  value = aws_db_subnet_group.default.id
}