resource "aws_db_subnet_group" "default" {
  name_prefix = var.name_prefix
  subnet_ids  = var.subnet_ids

  tags = {
    Project     = var.project_tag
    Environment = var.environment_tag
  }
}