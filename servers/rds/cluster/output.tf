output "arn" {
  value = aws_rds_cluster.default.arn
}

output "id" {
  value = aws_rds_cluster.default.id
}

output "endpoint" {
  value = aws_rds_cluster.default.endpoint
}
