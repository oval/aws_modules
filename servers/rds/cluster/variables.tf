variable "region" {
  description = "The region to search for availability zones"
  type        = string
}

variable "cluster_identifier_prefix" {
  description = "A cluster identifier prefix"
  type        = string
}

variable "db_cluster_parameter_group_name" {
  description = "A db cluster parameter group"
  type        = string
}

variable "db_subnet_group_name" {
  description = "A DB subnet group"
  type        = string
}

variable "kms_key_id" {
  description = "A KMS key ID"
  type        = string
}

variable "master_password" {
  description = "A master password"
  type        = string
  sensitive   = true
}

variable "master_username" {
  description = "A master username"
  type        = string
  sensitive   = true
}

variable "security_group_ids" {
  description = "A list of security group ids"
  type        = list(string)
}

variable "project_tag" {
  description = "A project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "db_instance_class" {
  description = "Instance type to use"
  type        = string
}

variable "num_instances" {
  description = "Number of instances to create"
  type        = number
}