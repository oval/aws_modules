data "aws_availability_zones" "available" {
  state = "available"
  filter {
    name   = "region-name"
    values = [var.region]
  }
}

resource "aws_rds_cluster_instance" "cluster_instances" {
  count              = var.num_instances
  cluster_identifier = aws_rds_cluster.default.id
  instance_class     = var.db_instance_class
  engine             = aws_rds_cluster.default.engine
  engine_version     = aws_rds_cluster.default.engine_version
}

resource "aws_rds_cluster" "default" {
  availability_zones              = data.aws_availability_zones.available.names
  cluster_identifier_prefix       = var.cluster_identifier_prefix
  copy_tags_to_snapshot           = true
  db_cluster_parameter_group_name = var.db_cluster_parameter_group_name
  db_subnet_group_name            = var.db_subnet_group_name
  deletion_protection             = true
  enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
  engine                          = "aurora-mysql"
  engine_version                  = "5.7.mysql_aurora.2.07.7"
  kms_key_id                      = var.kms_key_id
  master_password                 = var.master_password
  master_username                 = var.master_username
  backup_retention_period         = 7
  preferred_backup_window         = "22:00-00:00"
  preferred_maintenance_window    = "FRI:20:00-FRI:22:00"
  storage_encrypted               = true
  vpc_security_group_ids          = var.security_group_ids
  
  tags = {
    Project     = var.project_tag
    Environment = var.environment_tag
  }
}