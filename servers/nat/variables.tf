variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "nat_ami" {
  description = "An AMI to use for the NAT instance"
  type        = string
}

variable "nat_instance_type" {
  default     = "t3a.nano"
  description = "An instance type to use for the NAT instance, default = t3a.nano"
  type        = string
}

variable "subnet_id" {
  description = "A public subnet ID to locate the NAT instance"
  type        = string
}

variable "security_group_ids" {
  description = "A list of security group IDs to assign the to NAT instance"
  type        = set(string)
}

variable "key_name" {
  description = "The SSh Key to use with the instance. This needs to be created in advance"
  type        = string
}