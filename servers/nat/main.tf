resource "aws_instance" "nat" {
  ami                         = var.nat_ami
  instance_type               = var.nat_instance_type
  associate_public_ip_address = true
  source_dest_check           = false
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = var.security_group_ids
  key_name                    = var.key_name
  tags                        = {
      Project = var.project_tag
      Environment = var.environment_tag
  }
}