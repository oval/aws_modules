output "instance_id" {
    value = aws_instance.nat.id
}

output "instance_ip" {
    value = aws_instance.nat.public_ip
}

output "instance_arn" {
    value = aws_instance.nat.arn
}

output "key_name" {
    value = var.key_name
}