resource "aws_cloudwatch_event_target" "example" {
  arn   = var.target_arn
  rule  = var.rule_id
  input = var.input_json
}