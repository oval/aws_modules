variable "target_arn" {
  description = "The lambda arn that the event will trigger"
  type        = string
}

variable "rule_id" {
  description = "The Event rule that the event will be attached to"
  type        = string
}

variable "input_json" {
  description = "The json data that will be sent to the lambda function"
  type        = string
}