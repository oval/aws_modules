variable "name_prefix" {
  description = "A name prefix for the rule"
  type       = string
}

variable "schedule_expression" {
  description = "A schedule expression for the rule i.e. cron(0 20 * * ? *) or rate(5 minutes)"
  type       = string
}

variable "description" {
  description = "A description for the rule"
  type       = string
}

variable "role_arn" {
  description = "A role arn to associate with the rule for target invocation"
  type       = string
}

variable "is_enabled" {
  default    = true
  description = "Is the rule enabled"
  type       = bool
}

variable "project_tag" {
  description = "A project tag"
  type       = string
}

variable "environment_tag" {
  description = "An environment tag"
  type       = string
}
