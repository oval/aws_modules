resource "aws_cloudwatch_event_rule" "main" {
  name_prefix         = var.name_prefix
  schedule_expression = var.schedule_expression
  description         = var.description
  role_arn            = var.role_arn
  is_enabled          = var.is_enabled
  tags                = {
    Project     = var.project_tag
    Environment = var.environment_tag
  }
}