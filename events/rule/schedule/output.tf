output "id" {
  value = aws_cloudwatch_event_rule.main.id
}

output "arn" {
  value = aws_cloudwatch_event_rule.main.arn
}