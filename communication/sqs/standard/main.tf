resource "aws_sqs_queue" "queue" {
  name_prefix                       = var.name_prefix
  kms_master_key_id                 = var.kms_master_key_id
  visibility_timeout_seconds        = var.visibility_timeout_seconds
  message_retention_seconds         = var.message_retention_seconds
  tags = {
    Project = var.project_tag
    Environment = var.environment_tag
  }
}