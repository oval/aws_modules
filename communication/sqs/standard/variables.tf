variable "name_prefix" {
  description = "A name prefix for the queu"
  type        = string
}

variable "visibility_timeout_seconds" {
  default     = 30
  description = "Message visibility timeout in seconds from 0 to 43200 (12 hours)"
  type        = number
}

variable "message_retention_seconds" {
  default     = 345600
  description = "The number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days). The default for this attribute is 345600 (4 days)"
  type        = number
}

variable "max_message_size" {
  default     = 262144
  description = "The limit of how many bytes a message can contain. The default for this attribute is 262144 (256 KiB)"
  type        = number
}

variable "kms_master_key_id" {
  description = "The Id of the KMS key"
  type        = string
}

variable "project_tag" {
  description = "The project tag"
  type        = string
}

variable "environment_tag" {
  description = "The environment tag"
  type        = string
}