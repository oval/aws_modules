resource "aws_subnet" "item" {
  count                   = length(var.availabilty_zone_ids)
  vpc_id                  = var.vpc_id
  availability_zone_id    = var.availabilty_zone_ids[count.index]
  cidr_block              = "10.0.${count.index+var.start_index}.0/24"
  map_public_ip_on_launch = var.is_public
  
  tags                    = {
    Name        = "${var.name_prefix} subnet"
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}