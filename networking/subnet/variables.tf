variable "availabilty_zone_ids" {
  description = "A list of availability zones ids to use for the subnets"
  type        = list(string)
}

variable "vpc_id" {
  description = "The vpc id to use"
  type        = string
}

variable "start_index" {
  description = "The subnet index to start at can be between 0-2"
  type        = string
}

variable "is_public" {
  description = "Is the subnet public"
  type        = bool
}

variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "name_prefix" {
  description = "A name prefix for the Name tag"
  type        = string
}