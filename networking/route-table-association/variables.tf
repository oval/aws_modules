variable "subnet_ids" {
  description = "A list of subnet IDs to associate with the route table"
  type        = list(string)
}

variable "route_table_id" {
  description = "The route table id to associate the subnets with"
  type        = string
}