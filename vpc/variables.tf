variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}

variable "key_name" {
  description = "The SSh Key to use with the instance. This needs to be created in advance"
  type        = string
}

variable "nat_ami" {
  description = "An AMI to use for the NAT instance"
  type        = string
}

variable "nat_instance_type" {
  default     = "t3a.nano"
  description = "An instance type to use for the NAT instance, default = t3a.nano"
  type        = string
}

variable "name_prefix" {
  description = "An name prefix to add to created resources"
  type        = string
}

variable "office_ip" {
  description = "The office IP address"
  type        = string
}

variable "region" {
  description = "The region to create the resources in"
  type        = string
}