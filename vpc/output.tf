output "vpc_id" {
    value = aws_vpc.main.id
}

output "private_subnet_ids" {
    value = module.private_subnet.subnet_ids
}

output "public_subnet_ids" {
    value = module.public_subnet.subnet_ids
}

output "nat_id" {
    value = module.nat.instance_id
}

output "nat_ip" {
    value = module.nat.instance_ip
}

output "nat_arn" {
    value = module.nat.instance_arn
}

output "internal_sg_id" {
    value = aws_security_group.internal.id
}

output "internal_sg_name" {
    value = aws_security_group.internal.name
}

output "ssh_sg_id" {
    value = aws_security_group.ssh.id
}

output "ssh_sg_name" {
    value = aws_security_group.ssh.name
}

output "public_route_table" {
  value = aws_route_table.public.id
}

output "private_route_table" {
  value = aws_route_table.private.id
}