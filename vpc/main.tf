
data "aws_availability_zones" "available" {
  state = "available"
  filter {
    name   = "region-name"
    values = [var.region]
  }
}

locals {
  global_cidr_block = "0.0.0.0/0"
  availabilty_zone_ids = data.aws_availability_zones.available.zone_ids
}

#--------------------------------------------------------------------------------------------------
# VPC
#--------------------------------------------------------------------------------------------------

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}

#--------------------------------------------------------------------------------------------------
# Subnets
#--------------------------------------------------------------------------------------------------

module "private_subnet" {
  source = "../networking/subnet"

  availabilty_zone_ids = local.availabilty_zone_ids
  vpc_id               = aws_vpc.main.id
  environment_tag      = var.environment_tag
  project_tag          = var.project_tag
  name_prefix          = "Private"
  start_index          = 0
  is_public            = false
}

module "public_subnet" {
  source = "../networking/subnet"

  availabilty_zone_ids = local.availabilty_zone_ids
  vpc_id               = aws_vpc.main.id
  environment_tag      = var.environment_tag
  project_tag          = var.project_tag
  name_prefix          = "Public"
  start_index          = length(local.availabilty_zone_ids)
  is_public            = true
}

#--------------------------------------------------------------------------------------------------
# IGW
#--------------------------------------------------------------------------------------------------

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
  tags   = {
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}

#--------------------------------------------------------------------------------------------------
# Routing
#--------------------------------------------------------------------------------------------------

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  tags   = {
    Name = "Public RT"
    Project = var.project_tag
    Environment = var.environment_tag
  }
}

module "public_rt_assoc" {
  source = "../networking/route-table-association"

  route_table_id = aws_route_table.public.id
  subnet_ids     = module.public_subnet.subnet_ids
}

resource "aws_route" "public" {
  route_table_id         = aws_route_table.public.id
  gateway_id             = aws_internet_gateway.main.id
  destination_cidr_block = local.global_cidr_block
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id
  tags   = {
    Name = "Private RT"
    Project = var.project_tag
    Environment = var.environment_tag
  }
}

module "private_rt_assoc" {
  source = "../networking/route-table-association"

  route_table_id = aws_route_table.private.id
  subnet_ids     = module.private_subnet.subnet_ids
}

resource "aws_route" "private" {
  route_table_id         = aws_route_table.private.id
  instance_id            = module.nat.instance_id
  destination_cidr_block = local.global_cidr_block
}

#--------------------------------------------------------------------------------------------------
# NAT Instance
#--------------------------------------------------------------------------------------------------

module "nat" {
  source = "../servers/nat"

  key_name           = var.key_name
  nat_ami            = var.nat_ami
  nat_instance_type  = var.nat_instance_type
  subnet_id          = element(module.public_subnet.subnet_ids, 0)
  security_group_ids = [aws_security_group.internal.id, aws_security_group.ssh.id]
  project_tag        = var.project_tag
  environment_tag    = var.environment_tag
}

#--------------------------------------------------------------------------------------------------
# Network ACLs
#--------------------------------------------------------------------------------------------------

resource "aws_network_acl" "default" {
  vpc_id     = aws_vpc.main.id
  subnet_ids = concat(module.private_subnet.subnet_ids, module.public_subnet.subnet_ids)

  egress {
    from_port  = 0
    to_port    = 0
    rule_no    = 100
    protocol   = "-1"
    action     = "allow"
    cidr_block = local.global_cidr_block
  }

  ingress {
    from_port  = 0
    to_port    = 0
    rule_no    = 100
    protocol   = "-1"
    action     = "allow"
    cidr_block = local.global_cidr_block
  }
}

#--------------------------------------------------------------------------------------------------
# Default Security Groups
#--------------------------------------------------------------------------------------------------

resource "aws_security_group" "internal" {
  name_prefix = "${var.name_prefix}-internal"
  vpc_id      = aws_vpc.main.id

  tags = {
    Name        = "Internal SG"
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}

resource "aws_security_group_rule" "internal_ingress" {
  type              = "ingress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["10.0.0.0/16"]
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group_rule" "internal_egress" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = [local.global_cidr_block]
  security_group_id = aws_security_group.internal.id
}

resource "aws_security_group" "ssh" {
  name_prefix = "${var.name_prefix}-ssh"
  vpc_id      = aws_vpc.main.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.office_ip]
    self        = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [local.global_cidr_block]
  }

  tags = {
    Name        = "SSH SG"
    Environment = var.environment_tag
    Project     = var.project_tag
  }
}

