# Regional S3 Bucket

Creates an S3 bucket using default AWS KMS S3 encryption keys. This bucket will be generated in a specific region provided by the alternative aws provider
