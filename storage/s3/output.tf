output "id" {
    value = aws_s3_bucket.item.id
}

output "arn" {
    value = aws_s3_bucket.item.arn
}