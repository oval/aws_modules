data "aws_canonical_user_id" "current" {}

resource "aws_s3_bucket" "item" {
  bucket_prefix = var.bucket_prefix

  tags = {
      Project = var.project_tag
      Environment = var.environment_tag
  }
}

resource "aws_s3_bucket_acl" "item" {
  bucket = aws_s3_bucket.item.id
  access_control_policy {
    grant {
      grantee {
        id   = data.aws_canonical_user_id.current.id
        type = "CanonicalUser"
      }
      permission = "FULL_CONTROL"
    }

    owner {
      id = data.aws_canonical_user_id.current.id
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "item" {
  bucket = aws_s3_bucket.item.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_versioning" "item" {
  bucket = aws_s3_bucket.item.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "versioning-bucket-config" {
  depends_on = [aws_s3_bucket_versioning.item]

  bucket = aws_s3_bucket.item.id

  rule {
    id = "legacyVersions"

    noncurrent_version_expiration {
      noncurrent_days = 31
    }

    status = "Enabled"
  }
}