variable "bucket_prefix" {
    description = ""
}

variable "acl" {
  default     = "private"
  description = "The ACL level to apply to the bucket. Options: private, public-read, public-read-write, aws-exec-read, authenticated-read, log-delivery-write"
  type        = string
  validation {
    # regex(...) fails if it cannot find a match
    condition     = can(regex("(private|public-read|public-read-write|aws-exec-read|authenticated-read|log-delivery-write)", var.acl))
    error_message = "The region value must be one of 'private', 'public-read', 'public-read-write', 'aws-exec-read', 'authenticated-read', 'log-delivery-write'."
  }
}

variable "project_tag" {
  description = "A Project tag"
  type        = string
}

variable "environment_tag" {
  description = "An environment tag"
  type        = string
}