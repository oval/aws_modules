data "aws_canonical_user_id" "current" {}
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}


data "aws_iam_policy_document" "s3_policy" {
  count = var.distribution_id != "" ? 1 : 0
  statement {
    effect = "Allow"
    actions = [ "s3:GetObject" ]
    resources = ["${aws_s3_bucket.item.arn}/*"]
    principals {
      type = "Service"
      identifiers = [ "cloudfront.amazonaws.com" ]
    }
    condition {
      test = "StringEquals"
      variable = "AWS:SourceArn"
      values = [ "arn:aws:cloudfront::${data.aws_caller_identity.current.account_id}:distribution/${var.distribution_id}" ]
    }
  }
}

resource "aws_s3_bucket_policy" "item" {
  count = var.distribution_id != "" ? 1 : 0

  bucket = aws_s3_bucket.item.id
  policy = data.aws_iam_policy_document.s3_policy[count.index].json
}

data "aws_iam_policy_document" "kms_role" {
  statement {
    effect = "Allow"
    actions = [ "sts:AssumeRole" ]
    principals {
      type = "AWS"
      identifiers = [ data.aws_caller_identity.current.account_id ]
    }
  }
}

module "kms_role" {
  source = "../../iam/role"

  name_prefix     = "${var.bucket_prefix}-kms-role"
  policy_arns     = ["arn:aws:iam::aws:policy/AWSKeyManagementServicePowerUser"]
  project_tag     = var.project_tag
  environment_tag = var.environment_tag

  assume_role_policy_document = data.aws_iam_policy_document.kms_role.json
}

data "aws_iam_policy_document" "kms_policy" {
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    actions = [ "kms:*" ]
    principals {
      type = "AWS"
      identifiers = [ module.kms_role.arn ]
    }
    principals {
      type = "AWS"
      identifiers = [ 
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
      ]
    }
    resources = [ "*" ]
  }

  statement {
    sid    = "Allow access for Key Administrators"
    effect = "Allow"
    actions = [
      "kms:Create*",
      "kms:Describe*",
      "kms:Enable*",
      "kms:List*",
      "kms:Put*",
      "kms:Update*",
      "kms:Revoke*",
      "kms:Disable*",
      "kms:Get*",
      "kms:Delete*",
      "kms:TagResource",
      "kms:UntagResource",
      "kms:ScheduleKeyDeletion",
      "kms:CancelKeyDeletion"
    ]
    principals {
      type = "AWS"
      identifiers = [ 
        "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/OvalAccessRole"
      ]
    }
    resources = [ "*" ]
  }

  statement {
    sid    = "Allow use of the key"
    effect = "Allow"
    actions = [ 
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    principals {
      type = "AWS"
      identifiers = [ "*" ]
    }
    condition {
      test     = "StringEquals"
      variable = "kms:CallerAccount"
      values   = [ data.aws_caller_identity.current.account_id ]
    }
    condition {
      test     = "StringEquals"
      variable = "kms:ViaService"
      values   = [ "s3.${data.aws_region.current.name}.amazonaws.com" ]
    }
    resources = [ "*" ]
  }

  statement {
    sid    = "Allow CloudFront use of the key"
    effect = "Allow"
    actions = [ 
      "kms:Encrypt",
      "kms:Decrypt",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:DescribeKey"
    ]
    principals {
      type = "Service"
      identifiers = [ "cloudfront.amazonaws.com" ]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:SourceArn"
      values   = [ "arn:aws:cloudfront::${data.aws_caller_identity.current.account_id}:distribution/${var.distribution_id}" ]
    }
    resources = [ "*" ]
  }
}

module "kms_key" {
  source = "../../kms"

  name = aws_s3_bucket.item.id
  key_policy = data.aws_iam_policy_document.kms_policy.json
}

resource "aws_s3_bucket" "item" {
  bucket_prefix = var.bucket_prefix

  tags = {
      Project = var.project_tag
      Environment = var.environment_tag
  }
}

resource "aws_s3_bucket_acl" "item" {
  bucket = aws_s3_bucket.item.id
  access_control_policy {
    grant {
      grantee {
        id   = data.aws_canonical_user_id.current.id
        type = "CanonicalUser"
      }
      permission = "READ"
    }

    owner {
      id = data.aws_canonical_user_id.current.id
    }
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "item" {
  bucket = aws_s3_bucket.item.bucket
  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "aws:kms"
      kms_master_key_id = module.kms_key.key_id
    }
  }
}

resource "aws_s3_bucket_versioning" "item" {
  bucket = aws_s3_bucket.item.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "versioning-bucket-config" {
  depends_on = [aws_s3_bucket_versioning.item]

  bucket = aws_s3_bucket.item.id

  rule {
    id = "legacyVersions"

    noncurrent_version_expiration {
      noncurrent_days = 31
    }

    status = "Enabled"
  }
}