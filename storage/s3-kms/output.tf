output "id" {
    value = aws_s3_bucket.item.id
}

output "arn" {
    value = aws_s3_bucket.item.arn
}

output "kms_key_id" {
  value = module.kms_key.key_id
}