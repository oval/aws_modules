output "id" {
    value = aws_s3_bucket.website.id
}

output "arn" {
    value = aws_s3_bucket.website.arn
}

output "domain" {
    value = aws_s3_bucket.website.bucket_domain_name
}