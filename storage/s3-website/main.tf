resource "aws_s3_bucket" "website" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_acl" "item" {
  bucket = aws_s3_bucket.website.id
  acl = "public-read"
}

resource "aws_s3_bucket_policy" "item" {
  bucket = aws_s3_bucket.website.id

  policy = var.policy
}

resource "aws_s3_bucket_website_configuration" "item" {
  bucket = aws_s3_bucket.website.id
  index_document {
    suffix = var.index_document
  }
  error_document {
    key = var.error_document
  }
}

resource "aws_s3_bucket_cors_configuration" "item" {
  bucket = aws_s3_bucket.website.id
  cors_rule {
    allowed_headers = var.allowed_headers
    allowed_methods = var.allowed_methods
    allowed_origins = var.allowed_origins
    max_age_seconds = var.max_age_seconds
  }
}