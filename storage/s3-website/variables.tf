variable "bucket_name" {
  description = "A unique name for the bucket"
  type        = string
}

variable "policy" {
  description = "A json encoded bucket policy"
  type        = string
}

variable "index_document" {
  default     = "index.html"
  description = "The name of the website index document. Defaul = 'index.html'"
  type        = string
}

variable "error_document" {
  default     = "index.html"
  description = "The name of the error document for the website to use. Default = index.html"
  type        = string
}

variable "allowed_headers" {
  default     = ["*"]
  description = "A list of allowed headers for the website. Default = [\"*\"]"
  type        = list(string)
}

variable "allowed_methods" {
  default     = ["GET", "HEAD"]
  description = "A list of allowed methods for the website, can include 'GET', 'PUT', 'POST', 'DELETE', 'HEAD'. Default = [\"GET\", \"HEAD\"]"
  type        = list(string)
}

variable "allowed_origins" {
  default     = ["*"]
  description = "A list of allowed origins for the website. Default = [\"*\"]"
  type        = list(string)
}

variable "max_age_seconds" {
  default     = 3000
  description = "Specifies time in seconds that browser can cache the response for a preflight request. Default = 3000"
  type        = string
}